DailyDialogue Analysis 

DailyDialogue is a professionaly manualy labelled multi-turn text dialogue dataset. The language is
human-written. Dataset covers topics topics from daily life. Dialogues are also
labelled with communication and emotion information. Dataset is not domain-specific
nor task oriented. 

Data are colected from websites which serve for the English learners to practice
English dialogue in daily life. Most conversations focus on a certain topic and are
under certain physical context. Dialogues have comparably smaller number of turns,
aproximately 8 turns on average. Every dialogue involves two parties. 

Total dialogues					13 118
Average speaker turns/dialogue	7.9
Average tokens/dialogue 		114.7
Average tokens/utterance		14.6
Vocabulary size roughly			25 000

Each utterance is labelled in one of the classes: Inform, Questions, Directives, Commissive
Each utterance is labelled in one of: Anger, Disgust, Fear, Happiness, Sadness, Surprise, Other
Each dialogue is labelled by its topic into one of the categories: Ordinary life, School life,
Culture and Education, Attitude and Emotion, Relationship, Tourism, Health, Work, Politics, Finance

Dataset is intended to be used for future research on developing conversational agents able 
to regulate the conversation towards a happy ending. It is also intended to be used for
text based dialogue systems focused on question-answer and directives-commisive bi-turn dialogues. 
It is also promising to utilize the topic information in the dataset by domain adaptation and
transfer learning.

Format:
1) dialogues_text.txt: The DailyDialog dataset which contains 11,318 transcribed dialogues. Each on one line. 
					   Utteraces end with tag  "__eou__".
2) dialogues_topic.txt: Each line in dialogues_topic.txt corresponds to the topic of that in dialogues_text.txt.
                        The topic number represents: {1: Ordinary Life, 2: School Life, 3: Culture & Education,
                        4: Attitude & Emotion, 5: Relationship, 6: Tourism , 7: Health, 8: Work, 9: Politics, 10: Finance}
3) dialogues_act.txt: Each line in dialogues_act.txt corresponds to the dialog act annotations in dialogues_text.txt.
                      The dialog act number represents: { 1: inform，2: question, 3: directive, 4: commissive }
4) dialogues_emotion.txt: Each line in dialogues_emotion.txt corresponds to the emotion annotations in dialogues_text.txt.
                          The emotion number represents: { 0: no emotion, 1: anger, 2: disgust, 3: fear, 4: happiness,
						  5: sadness, 6: surprise}
5) train.zip, validation.zip and test.zip are two different segmentations of the whole dataset. 


Licence

    The original copyright of all the conversations belongs to the source owner.

    The copyright of annotation belongs to our group, and they are free to the public.

    The dataset is only for research purposes. Without permission, it may not be used for any commercial purposes.



Vocabulary size: 19698 (after excluding digits and non-semantic standalone characters)
Word entropy: 6.42
Total data lenght is: 
         13118 dialogues
         102980 turns
         170067 sentences
         1167070 words
Each dialouge has on average: 
          7.85 turns with standard deviation  3.99
         12.96 sentences with standard deviation  7.94
         88.97 words with standard deviation 63.89

Data looks natural and rather formal due to the nature of its origin. There were some minor spelling errors.
I think, that the professional anotation, ralatively short length of each dialogue, emotionaly rich dialogues,
the question/answer directives/commisive flow of conversation make the dataset easy to learn from for systems
designed for short aimless chit-chat (such as in daily life). Because the dataset is not domain specific, I
doubt that the system could learn from it any useful knowledge, give helpful advice or instructions. What the
system could learn, however, is the general flow of the conversation and sentiment awareness. This could be 
useful in pretraining to later transfer learning to specific domain.  

