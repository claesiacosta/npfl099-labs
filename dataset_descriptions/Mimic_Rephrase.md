Mimic and rephrase dataset is developed to design systems that can communicate effectively.The provided under the MIT License developed by Eloquent Labs in 2019. 

The data was collected via crowdsourcing using a web interface. In the first phase of collection, the users were asked to generate prompts of variable lengths and structures. In the second phase, another set of users were asked to answer (Mimic Re-phrasal or **MR**) these prompts. Users were paid $0.10 per sentence for prompts and $0.07 per sentence in response generation. 

The dataset is split into two sets: **Emotive** and **IDONTKNOW**. Emotive dataset is for expressing sympathy in a positive or negative way. IDONTKNOW dataset is for answering questions you do not know anything about. I will mainly focus on Emotive dataset in this report.

Emotive dataset or sentiment dataset is labeled with a positive or negative sentiment. The sentiment annotation was human labelled. Sentiment data is stored under .csv and .tsv formats. Each prompt has two variant of replies: Full and condensed. The dataset can be imported easily using pandas dataframe.

You can get the structure of the dataset if you import sentiment.tsv.

|   | A                                                  | B                                          | Sentiment | Full_Condensed          |
|----|----------------------------------------------------|----------------------------------------------------|----------------|-----------|
| 0  | I just had a car accident                          | I am sorry to hear about your accident             | neg            | condensed |
| 1  | I just had a car accident                          | I am sorry to hear you had a car accident          | neg            | full      |
| 2  | I am feeling sick today                            | I am sorry you are sick                            | neg            | condensed |
| 3  | I am feeling sick today                            | I am sorry you are feeling sick today              | neg            | full      |
| 4  | I failed my history test                           | I am sorry you failed your test                    | neg            | condensed |
| 5  | I failed my history test                           | I am sorry you failed your history test            | neg            | full      |
| 6  | My cat died and I was so upset and cried so ha...  | I am sad your cat died                             | neg            | condensed |
| 7  | My cat died and I was so upset and cried so ha...  | I am sad your cat died and you are so upset be...  | neg            | full      |
| 8  | The mechanic bill for my car really disrupted ...  | I am sorry to hear about your mechanic bill        | neg            | condensed |
| 9  | The mechanic bill for my car really disrupted ...  | I am sorry to hear that the mechanic bill for ...  | neg            | full      |

Table below describes the dataset statistically and shows combined analysis of Full and Condensed dialouges: 

|  | Values |
| ------ | ------ |
| Shape of Dataset | (10726, 4) |
| Words in Prompts (A) | 107625 |
| Words in MR (B) | 110352 |
| Prompt mean token length | 10.3 |
| MR mean token length | 10.2 |
| Standard deviation of prompts | 3.69 |
| Standard deviation of MR | 3.19 |
| Total (Shannon) Entropy | 17.833 | 
| Total Negative Sentiments | 5395 |
| Total Positive Sentiments | 5331 | 

## Impression: 
Overall, Emotive dataset is pretty simple. The sentences are not so complex. We can use either full mimic rephrasels or condensed ones. Grammatical correctness of this dataset is about 88% (according to the dataset description paper). The neural Seq2Seq models and Rule based models reach human level performances through this dataset. As far as the scope is concerned, scope of this dataset is pretty narrow as it can only be used for developing systems that communicate sympathy. 

## References: 
Mimic and Rephrase: Reflective listening in open-ended dialogue [ https://www.aclweb.org/anthology/K19-1037.pdf ]