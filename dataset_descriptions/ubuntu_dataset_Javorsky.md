# Overview

The Ubuntu Dialog Corpus dataset is created by extracting dyadic dialoges from the chat room multi-party conversations which have been collected in Ubuntu chat logs form 2004 to 2015.

# Domain

The dataset consists of human-human chat conversations within a specific domain, namely technical support in Ubuntu environment.

# Collection

The dialogues were extracted from the Ubuntu Chat Logs which refers to a collection of logs from Ubuntu-related chat rooms on the Freenode Internet Relay Chat (IRC) network.

# Target

The dataset provides a resource for research into building dialogue managers based on neural language models. The task is to select the best response given a context.

# Annotation

The annotation was performed automaticly, a thorough description is present in the paper.

# Format

The dataset is divided into 3 files; train.csv, valid.csv and test.csv.

### train.csv

It contains the training set. It is separated into 3 columns: the context of the conversation, the candidate response and a flag (= 0 or 1) denoting whether the response is true in the given contxt or not.

### valid.csv

It contains the validation set. Each row represents a question and is separated into 11 columns: the context, the true response, and 9 false responses that were randomly sampled from elsewhere in the dataset.

### test.csv

It contains the test set. It is formatted in the same way as the validation set.

# Licence

Apache License 2.0

# Personal experience

The format of dialogues is straightforward and easy to work with. Each dialog file is consisted of four columns &mdash; the time of the utterance, the sender, the recipient and the response. I have encountered only one minor issue &mdash; some of turns had wrong format, in particular the number of columns was less than four.

However, the paper and github page was clear.

# Measurements
The last four lines are computed by myself given a smaller part of the dataset, the rest is directly mentioned in the paper. 

| Parameter                   | Value          |
| :-------------------------- | -------------: |
| # dialogues                 | 930,000        |
| # utterances                | 7,100,000      |
| # words                     | 100,000,000    |
| Min. # turns per dialogue   | 3              |
| Avg. # turns per dialogue   | 7.71           |
| Avg. # words per turn       | 10.34          |
| *Vocabulary size*           | *82397*        |
| *Avg. # words per turn*     | *10.06*        |
| *Avg. # turns per dialogue* | *20*           |
| *Entropy*                   | *6.01*         |
