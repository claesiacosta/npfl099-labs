Lab Projects
============

The lab semester projects will be your main task this year, and will be (along with the dataset homework)
what you'll get your lab credit for.

All projects have to fulfill our minimum requirements, but you're of course free to bring in your own ideas
and go above the minimum. If you like, we can plan & discuss submitting a conference paper.

You can also team up and create a bigger project -- but this means that the requirements will be higher
for the team than for an individual project. 

We can also team up two projects if they're complementary (e.g. RL-based dialogue manager and user simulator).

There will be further talks on conducting experiments and writing reports, and we'll happy to discuss
any of the steps with you.

Rules
-----

### Topic

* The topic needs to be something related to dialogue systems -- either an individual dialogue system component 
(language understanding, dialogue management, language generation, speech recognition or speech synthesis),
or an end-to-end dialogue system.

* The topic needs to involve machine learning.

* There needs to be at least 2 variants of your models -- one of them should preferrably be a "standard" baseline,
  such as seq2seq with attention, transformer, or e.g. SVM with bigram features only. The other variant should
  present a modification aiming for some improvement (which may or may not happen later, but you should aim for it).

  * You're obviously free to try out more variants of the model.

  * Don't make the baseline too different from your “better” model -- it would make the comparsion of the results
   harder. 


### Project Description

The project description should be submitted in Markdown into this repository (create a file in the [projects/](projects/)
 directory) and should show:

* What component you'll implement
* What model architectures you'll use -- a short description of how the model architectures will look like
  (for all the model variants), with citation(s) of relevant paper(s)
* What dataset(s) you'll use, any potential modifications required for the dataset
* What evaluation metrics you'll use

### Experiments

The experiments should be implemented in the [Dialmonkey framework](https://gitlab.com/ufal/dsg/dialmonkey/). 
Create your own fork and commit all experimental code there. Please implement your dialogue system components 
within the `dialmonkey.<component-type>` package, and include configuration files for all your system variants 
in the `conf` directory.

If you require GPUs for your project, you can try [Google Colab](https://colab.research.google.com/), or we 
can get you an account on the [ÚFAL student cluster](https://aic.ufal.mff.cuni.cz/index.php/Main_Page).

You're free to use Jupyter or whatever kind of scripting to automate the experiments, but the implemented 
components should run within Dialmonkey itself.

Submit your experimental part by linking to your experimental code from the project description Markdown in this
repository.

Commit your trained models using Git-LFS (or suggest an alternative, but we need to approve it).


### Reports

The report should be a short (min. 3 pages) PDF formatted according 
to the [EMNLP 2020 template](https://2020.emnlp.org/files/emnlp2020-templates.zip).

It should contain an updated description of your system, a short description of relevant related works,
your experiments and results.

The results should include a manual error analysis on 50 instances at least.

Use [Overleaf](https://overleaf.com/) or your own Git repository for the paper source. We'll first discuss the report, and you'll
only commit the final version PDF into this repository.


Project ideas
-------------

### Language Understanding

* Joint LSTM-CRF for intent & slot tagging after [Liu & Lane, 2016](http://arxiv.org/abs/1609.01454)
* CNN-based NLU after [Gupta et al., 2019](http://arxiv.org/abs/1903.08268)
* BERT-based intent & slot tagging after [Chen et al., 2019](http://arxiv.org/abs/1902.10909)

### Dialogue Management

* A classifier tracker (LSTM/CNN + per-slot classification) similar to [Mrkšić et al., 2017](https://www.aclweb.org/anthology/P17-1163)
* BERT-based tagging tracker after [Chao & Lane, 2019](http://arxiv.org/abs/1907.03040)
* Reading comprehension BERT tracker after [Lee et al., 2019](http://arxiv.org/abs/1907.07421) or [Gao et al., 2019](https://www.aclweb.org/anthology/W19-5932/)
* Deep Q Nets policy after [Li et al., 2017](https://arxiv.org/abs/1608.05081)
* Policy gradients policy after [Su et al., 2017](http://arxiv.org/abs/1707.00130)

### Language Generation

* A Copy/Pointer network seq2seq generator (similar to [Zhao & Feng, 2018](https://www.aclweb.org/anthology/P18-2068/)'s NLU)
* NLG-NLU combo ([Nie et al., 2019](https://www.aclweb.org/anthology/P19-1256), [Su et al., 2019](http://arxiv.org/abs/1905.06196))
* BERT-based few-shot NLG after [Chen et al., 2020](https://www.aclweb.org/anthology/2020.acl-main.18/)

### End-to-end Task Oriented

* Hybrid Code Network after [Williams et al., 2017](http://arxiv.org/abs/1702.03274)
* Sequicity -- two-stage copy network with explicit state after [Liu et al., 2018](https://www.aclweb.org/anthology/P18-1133)
* GPT-2-based system after [Budzianowski & Vulić, 2019](https://www.aclweb.org/anthology/D19-5602)

### End-to-end Non-task Oriented

* Hierarchical LSTM after [Lowe et al., 2017](http://dad.uni-bielefeld.de/index.php/dad/article/view/3698)

### User Simulation

* Rule-based simulator, tested with a basic seq2seq/transformer model (or a DM built by one of your colleagues)
* Hybrid Code Network (see end-to-end task oriented systems)
* Dual-optimized agent & simulator after [Liu & Lane, 2017](http://arxiv.org/abs/1709.06136)


Datasets and Metrics
--------------------

Feel free to use any dataset listed on the [datasets page](datasets.md). If you don't have any preference,
go with [MultiWoZ](https://github.com/budzianowski/multiwoz) (for task-oriented system), which we're most familiar with.

Use metrics appropriate for the task; refer to the [metrics lecture](https://ufal.mff.cuni.cz/~odusek/2020/docs/NPFL099_2020_03-eval_0.pdf) for more details.


Deadlines
---------

* 10 Nov -- Project topic
* 24 Nov -- Project description
* 12 Jan -- Experiments (tentative)
* 16 Feb -- Reports (tentative)

We're happy to extend the deadlines should you need it, but any extension needs to be agreed before the deadline itself.

Project Assignment
------------------

| **Name** | **Project topic** | **Description** | **Experiments** | **Report** |
|---------------------|:-:|:-:|:-:|:-:|
| Anna   |  |
| Claésia | (extension: Dec 1) | (extension: Dec 15) |
| Daniel | Weight Pruning for Transformer-based Models in Dialogue Generation |
| Dávid | THEaiTRE – chatbots for theatre plays |
| Dominik | THEaiTRE – chatbots for theatre plays |
| František | BERT-based few-shot NLG (after [Chen et al. 2020](https://www.aclweb.org/anthology/2020.acl-main.18/)) |
| Christián | THEaiTRE – chatbots for theatre plays |
| Lorcan |  |
| Michael | Reading comprehension BERT-based dialogue state tracking (after [Gao et al., 2019](https://www.aclweb.org/anthology/W19-5932/)) |
| Patrícia | THEaiTRE – chatbots for theatre plays |
| Saad |  |
| Samuel |  |
| Souro |  |
| Tomáš J. |  |
| Tomáš S. |  |
| Veronika |  |
| Vladislav | BERT-based intent & slot tagging (after [Chen et al., 2019](http://arxiv.org/abs/1902.10909)) |
